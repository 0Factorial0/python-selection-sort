def selection_sort():

    import random

    #get input
    list0 = []
    list_length = int(input("Input List Length: "))

    #generate numbers
    for i in range(0, list_length):
        list0.append(random.randint(0,100))

    #print unsorted list
    print("-------------")
    print("Unsorted List:",list0)
    print("-------------")
    
    #repeat for every index
    for i in range(0,len(list0)):
        min_index = i
        #find the minimum number
        for j in range(min_index, len(list0)):
            if list0[j] < list0[min_index]:
                min_index = j
        #swap the minimum number and index
        (list0[i],list0[min_index]) = (list0[min_index],list0[i])
    
    #print sorted list
    print("Sorted List:",list0)
    print("-------------")

selection_sort()
